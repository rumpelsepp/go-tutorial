package main

import (
	"fmt"
	"golang.org/x/tour/tree"
)

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	_walk_tree(t, ch)
	close(ch)
}

func _walk_tree(t *tree.Tree, ch chan int) {
	if t.Left != nil {
		_walk_tree(t.Left, ch)
	}

	ch <- t.Value

	if t.Right != nil {
		_walk_tree(t.Right, ch)
	}
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	ch1 := make(chan int, 10)
	ch2 := make(chan int, 10)

	go Walk(t1, ch1)
	go Walk(t2, ch2)

	for v1 := range ch1 {
		if <-ch2 != v1 {
			return false
		}
	}
	return true
}

func main() {
	ch := make(chan int)

	go Walk(tree.New(1), ch)

	// Prints the tree.
	for t := range ch {
		fmt.Println(t)
	}

	// The first one print "true", the second "false".
	fmt.Println(Same(tree.New(1), tree.New(1)))
	fmt.Println(Same(tree.New(1), tree.New(2)))
}
