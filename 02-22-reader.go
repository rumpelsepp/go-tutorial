package main

import "golang.org/x/tour/reader"

type MyReader struct{}

func (r MyReader) Read(b []byte) (int, error) {
	if len(b) == 0 {
		return 0, nil
	}

	for x := range b {
		b[x] = 'A'
	}

	return len(b), nil
}

func main() {
	reader.Validate(MyReader{})
}
