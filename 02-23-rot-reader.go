package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func rot13(c *byte) bool {
	if *c >= 'a' && *c < 'a'+13 || *c >= 'A' && *c < 'A'+13 {
		*c = *c + 13
		return true
	} else if *c <= 'z' && *c > 'z'-13 || *c >= 'Z' && *c < 'Z'+13 {
		*c = *c - 13
		return true
	}

	return false
}

func (rot *rot13Reader) Read(d []byte) (int, error) {
	n, err := rot.r.Read(d)

	for i := range d {
		rot13(&d[i])
	}

	if err != nil {
		return 0, err
	}

	return n, nil
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
